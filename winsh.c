#include <conio.h>
#include <direct.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <windows.h>
#include <winuser.h>
#include "common.h"

extern void historyPrevious(void);
extern void historyNext(void);
extern void pushHistory(void);

#define BACKSPACE 0x08
#define LINEFEED 0x0a
#define RETURN 0x0d
#define CSI "\x1b["
#define ESC 0x1b
#define DEL 0x7f

static HANDLE hStdin;
static char command[CHAR_MAX];
static int row, col;
static int pos, end;

const char *getCommand(void)
{
    return command;
}

void reportDeviceStatus(void)
{
    _cputs(CSI "6n");
}

void prompt(void)
{
    char buf[CHAR_MAX];
    _cprintf("%s$ ", _getcwd(buf, CHAR_MAX));
    reportDeviceStatus();
}

void resetCommand(const char *newCommand)
{
    strcpy(command, newCommand);
    _cputs(CSI "2K" CSI "G");
    prompt();
    _cputs(command);
    end = strlen(command);
    pos = end;
}

void init(void)
{
    hStdin = GetStdHandle(STD_INPUT_HANDLE); 
    if (hStdin == INVALID_HANDLE_VALUE) 
        perror("GetStdHandle");

    if (!SetConsoleMode(hStdin, ENABLE_INSERT_MODE | ENABLE_VIRTUAL_TERMINAL_INPUT | ENABLE_QUICK_EDIT_MODE)) {
        perror("SetConsoleMode");
    }

    HANDLE hStdout;
    hStdout = GetStdHandle(STD_OUTPUT_HANDLE); 
    if (hStdout == INVALID_HANDLE_VALUE) 
        perror("GetStdHandle");

    if (!SetConsoleMode(hStdout, ENABLE_PROCESSED_OUTPUT | ENABLE_WRAP_AT_EOL_OUTPUT | ENABLE_VIRTUAL_TERMINAL_PROCESSING)) {
        perror("SetConsoleMode");
    }
}

static KEY_EVENT_RECORD readKeyEvent(void)
{
    while (TRUE) {
        DWORD numRead;
        INPUT_RECORD input;
        if (!ReadConsoleInput(hStdin, &input, 1, &numRead)) {
            printf("Could not read input: %d\n", GetLastError());
            exit(1);
        }
        if (input.EventType != KEY_EVENT) {
            continue;
        }
        KEY_EVENT_RECORD keyEvent = input.Event.KeyEvent;
        if (!keyEvent.bKeyDown) {
            continue;
        }
        return keyEvent;
    }
}

static CHAR readCharacter(void)
{
    return readKeyEvent().uChar.AsciiChar;
}

void updateCursorPosition(void)
{
    char buf[10];
    _snprintf(buf, 10, CSI "%dG", col+pos);
    _cputs(buf);
}

void goBackward(int count) {
    if (pos > 0) {
        pos -= count;
        if (pos < 0) {
            pos = 0;
        }
        updateCursorPosition();
    }
}

void goForward(int count) {
    if (pos < end) {
        pos += count;
        if (pos > end) {
            pos = end;
        }
        updateCursorPosition();                
    }
}

static int isWordStart(int i)
{
    return i > 0 && isalnum(command[i]) && !isalnum(command[i-1]);
}

static int nextWordStart(void)
{
    int next = pos - 1;
    while (next > 0 && !isWordStart(next)) {
        next--;
    }
    return next;
}

static void goBackwardWord(void) {
    if (pos > 0) {
        pos = nextWordStart();
        updateCursorPosition();
    }
}

static int isWordEnd(int i)
{
    return i > 0 && isalnum(command[i-1]) && !isalnum(command[i]);
}

static int nextWordEnd(void)
{
    if (pos == end) {
        return end;
    }
    int next = pos + 1;
    while (next < end && !isWordEnd(next)) {
        next++;
    }
    return next;
}

static void goForwardWord(void) {
    if (pos < end) {
        pos = nextWordEnd();
        updateCursorPosition();
    }
}

void goToStart(void)
{
    pos = 0;
    updateCursorPosition();
}

void goToEnd(void)
{
    pos = end;
    updateCursorPosition();
}

static void deleteForward(int count);

static void deleteBackward(int count)
{
    if (count > pos) {
        count = pos;
    }
    if (count > 0) {
        pos -= count;
        updateCursorPosition();
        deleteForward(count);
    }
}

static int isSpaceStart(int i)
{
    return i > 0 && !isspace(command[i]) && isspace(command[i-1]);
}

static int nextSpaceStart(void)
{
    if (pos == 0) {
        return 0;
    }
    int next = pos - 1;
    while (next > 0 && !isSpaceStart(next)) {
        next--;
    }
    return next;
}

static void deleteBackwardWhitespace(void)
{
    int next = nextSpaceStart();
    deleteBackward(pos - next);
}

static void deleteBackwardWord(void)
{
    int next = nextWordStart();
    deleteBackward(pos - next);
}

static void deleteForward(int count)
{
    int maxCount = end - pos;
    if (count > maxCount) {
        count = maxCount;
    }
    if (count > 0) {
        for (int i = pos; i < end - count; i++) {
            command[i] = command[i + count];
        }
        end -= count;
        printf(CSI "%dP", count);
    }
}

static void deleteForwardOrExit()
{
    if (pos == 0 && end == 0) {
        processCommand("exit");
    } else {
        deleteForward(1);
    }
}

static void deleteForwardWord(void)
{
    int next = nextWordEnd();
    deleteForward(next - pos);
}

void insertCharacter(char character)
{
    if (end < CHAR_MAX-1) {
        for (int i = end; i >= pos; i--) {
            command[i+1] = command[i];
        }
        command[pos] = character;
        _cputs(CSI "@");
        _putch(character);
        pos++;
        end++;
    }
}

void clearToStart(void)
{
    deleteBackward(pos);
}

void clearScreen(void)
{
    _cputs(CSI "2J" CSI "H");
    prompt();
    _cputs(command);
    updateCursorPosition();
}

struct EscapeSequence {
    char code1;
    char code2;
    int arg1;
    int arg2;
};

#define ESC_ARG_MAX 2
#define ESC_ARG_MAX_LENGTH 5

int parseEscapeParameter(const char *buf)
{
    if (buf[0] == '\0') {
        return 1;
    }
    int arg = atoi(buf);
    if (arg < 1) {
        arg = 1;
    }
    return arg;
}

static void readEscapeSequence(struct EscapeSequence *esc) {
    ZeroMemory(esc, sizeof(*esc));
    esc->code1 = readCharacter();
    if (esc->code1 == '[') {
        char buf[ESC_ARG_MAX][ESC_ARG_MAX_LENGTH] = { "", "" };
        int argc = 0, i = 0;
        char code2;
        while (1) {
            char character = readCharacter();
            if (character >= '0' && character <= '9') {
                if (i >= ESC_ARG_MAX_LENGTH) {
                    _cputs("CSI sequence parse error: argument too long\n");
                    exit(1);
                }
                buf[argc][i++] = character;
            } else {
                if (argc >= ESC_ARG_MAX) {
                    _cputs("CSI sequence parse error: too many arguments\n");
                    exit(1);
                }
                buf[argc][i] = '\0';
                argc++;
                i = 0;
                if (character != ';') {
                    code2 = character;
                    break;
                }
            }
        }
        esc->code2 = code2;
        esc->arg1 = parseEscapeParameter(&buf[0][0]);
        esc->arg2 = parseEscapeParameter(&buf[1][0]);
    }
}

static void paste(void)
{
    HWND hwnd = GetConsoleWindow();
    if (!hwnd) {
        printf("Could not get Window handle.\n");
    }
    if (!OpenClipboard(hwnd)) {
        printf("Could not open clipboard (%d).\n", GetLastError());
    }
    HANDLE hglb = GetClipboardData(CF_TEXT);
    if (!hglb) {
        printf("GetClipboardData failed (%d).\n", GetLastError());
    }
    char *lptstr = GlobalLock(hglb);
    while (lptstr[0]) {
        insertCharacter(lptstr[0]);
        lptstr++;
    }
}

static void unknownEscapeSequence(const struct EscapeSequence *esc)
{
    printf("ESC%c%d;%d%c", esc->code1, esc->arg1, esc->arg2, esc->code2);
}

static void executeEscapeSequence(const struct EscapeSequence *esc)
{
    switch (esc->code1) {
        case 'b':
            goBackwardWord();
            return;
        case 'd':
            deleteForwardWord();
            return;
        case 'f':
            goForwardWord();
            return;
        case BACKSPACE:
        case DEL:
            deleteBackwardWord();
            return;
        case '[':
            switch (esc->code2) {
                case 'A':
                    historyPrevious();
                    return;
                case 'B':
                    historyNext();
                    return;
                case 'C':
                    goForward(esc->arg1);
                    return;
                case 'D':
                    goBackward(esc->arg1);
                    return;
                case 'F':
                    goToEnd();
                    return;
                case 'H':
                    goToStart();
                    return;
                case 'R':
                    row = esc->arg1;
                    col = esc->arg2;
                    return;
                case '~':
                    switch (esc->arg1) {
                        case 2:
                            paste();
                            return;
                        case 3:
                            deleteForward(1);
                            return;
                    }
            }
            break;
    }
    unknownEscapeSequence(esc);
}

static void processEscapeSequence(void)
{
    struct EscapeSequence esc;
    readEscapeSequence(&esc);
    executeEscapeSequence(&esc);
}

void readCommand(void)
{
    pos = 0;
    end = 0;
    while (1) {
        command[end] = '\0';
        CHAR character = readCharacter();
        //_cprintf("0x%02x", character);
        if (character == 0x00) {
            // do nothing
        } else if (character == ESC) {
            processEscapeSequence();
        } else if (character == 'a'-'a'+1) {
            goToStart();
        } else if (character == 'b'-'a'+1) {
            goBackward(1);
        } else if (character == 'd'-'a'+1) {
            deleteForwardOrExit();
        } else if (character == 'e'-'a'+1) {
            goToEnd();
        } else if (character == 'f'-'a'+1) {
            goForward(1);
        } else if (character == 'l'-'a'+1) {
            clearScreen();
        } else if (character == 'n'-'a'+1) {
            historyNext();
        } else if (character == 'p'-'a'+1) {
            historyPrevious();
        } else if (character == 'u'-'a'+1) {
            clearToStart();
        } else if (character == 'w'-'a'+1) {
            deleteBackwardWhitespace();
        } else if (character == BACKSPACE || character == DEL) {
            deleteBackward(1);
        } else if (character == RETURN || character == LINEFEED) {
            _cputs("\n");
            row++;
            break;
        } else {
            insertCharacter(character);
        }
        if (pos > end) {
            end = pos;
        }
    }
}

int main(void)
{
    init();
    while (1) {
        prompt();
        readCommand();
        pushHistory();
        processCommand(command);
    }
    return 0;
}