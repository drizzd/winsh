#include <malloc.h>
#include <stdio.h>

extern const char *getCommand(void);
extern void resetCommand(const char *newCommand);

#define HISTORY_SIZE 10
static char *history[HISTORY_SIZE];
static int historyTail;
static int historyPos;

static void setHistory(int slot)
{
    if (history[slot]) {
        free(history[slot]);
    }
    const char *command = getCommand();
    char *historyEntry = malloc(strlen(command));
    if (!historyEntry) {
        printf("Out of memory.");
        return;
    }
    strcpy(historyEntry, command);
    history[slot] = historyEntry;
}

static void restoreHistory(int slot)
{
    resetCommand(history[slot]);
}

static void historyStep(int direction)
{
    if (direction > 0 && historyPos == historyTail) {
        return;
    }

    int historyNext = direction > 0 ? historyPos + 1 : historyPos - 1;
    if (historyNext >= HISTORY_SIZE) {
        historyNext = 0;
    } else if (historyNext < 0) {
        historyNext = HISTORY_SIZE - 1;
    }

    if (direction < 0 && historyNext == historyTail) {
        return;
    }
    if (!history[historyNext]) {
        return;
    }

    setHistory(historyPos);
    historyPos = historyNext;
    restoreHistory(historyPos);
}

void historyPrevious(void)
{
    historyStep(-1);
}

void historyNext(void)
{
    historyStep(1);
}

void pushHistory()
{
    setHistory(historyTail);
    historyTail++;
    if (historyTail >= HISTORY_SIZE) {
        historyTail = 0;
    }
    historyPos = historyTail;
}
