# winsh

Winsh is a native command-line interpreter (shell) for Windows. The main idea
is to support bash/readline/Emacs-style command line editing in a cmd.exe-like
shell. The following key bindings are currently supported:

| Key | Command |
| --- | ------- |
| Ctrl-a | Move to the start of the current line.
| Ctrl-b | Move back a character.
| Ctrl-d | Delete the character underneath the cursor.
| Ctrl-e | Move to the ned of the line.
| Ctrl-f | Move forward a character.
| Ctrl-h | Delete the character to the left of the cursor.
| Ctrl-l | Clear the screen, reprinting the current line at the top.
| Ctrl-w | Kill from the cursor to the previous whitespace.
| Ctrl-u | Kill backward from the cursor to the beginning of the current line.
| Alt-DEL | Kill from the cursor the start of the current word, or, if between words, to the start of the previous word.
| Alt-b  | Move backward a word.
| Alt-d  | Kill from the cursor to the end of the current word, or, if between words, to the end of the next word.
| Alt-f  | Move forward a word, where a word is composed of letters and digits.
