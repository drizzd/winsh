#ifndef COMMON_H
#define COMMON_H

#ifndef CHAR_MAX
#define CHAR_MAX 8192
#endif

extern void processCommand(char *command);

#endif /* COMMON_H */