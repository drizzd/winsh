#include <direct.h>
#include <stdio.h>
#include <windows.h>
#include "common.h"

void processCommand(char *command)
{
    char *c = command;
    while (c[0] == ' ') {
        c++;
    }
    if (!c[0]) {
        return;
    }

    if (!strcmp(command, "exit")) {
        exit(0);
    }

    if (!strncmp(command, "cd", 2) && (command[2] == '\0' || command[2] == ' ')) {
        char *c = command + 2;
        while (c[0] == ' ') {
            c++;
        }
        if (c[0] != '\0') {
            if (_chdir(c)) {
                perror("chdir");
            }
        } else {
            char buf[CHAR_MAX];
            _cprintf("%s\n", _getcwd(buf, CHAR_MAX));
        }
        return;
    }

    if (!strcmp(command, "dir")) {
        strcpy(command, "cmd.exe /c dir");
    }

    STARTUPINFO si;
    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);
    PROCESS_INFORMATION pi;
    ZeroMemory(&pi, sizeof(pi));

    BOOL ret = CreateProcessA(NULL, command, NULL, NULL, TRUE, 0, NULL, NULL, &si, &pi);
    if (!ret) {
        DWORD err = GetLastError();
        switch (err) {
            case ERROR_FILE_NOT_FOUND:
                printf("Command not found.\n");
                break;
            default:
                printf("CreateProcess failed (%d).\n", GetLastError());
        }
        return;
    }

    WaitForSingleObject(pi.hProcess, INFINITE);

    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);
}
